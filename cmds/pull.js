/// <reference path="./types.d.ts" />
const yargs = require('yargs')
const shell = require('shelljs')
const yaml = require('js-yaml')
const fs = require('fs')
/**
 * @typedef {Object} argv
 * @prop {string} file
 */
module.exports = {
  command: 'pull',
  desc:'拉取 docker-compose.yml 里的镜像',
  /**@param {yargs} yargs */
  builder(yargs){
    return yargs
    .options('file',{ default:'/web/docker-compose.yml', type:'string', alias:'f' })
  },
  /**@param {argv} argv */  
  async corce(argv){ return argv },
  /**@param {argv} argv */  
  async handler(argv){
    argv = await require(__filename).corce(argv)
    let conf = /**@type {Types.DockerConfig}*/(yaml.safeLoad(fs.readFileSync(argv.file,'utf8')))
    // todo: 以后可以做成多进程下载的
    for(let name of Object.keys(conf.services)){
      let image = conf.services[name].image
      let pre = `下载服务 ${name} 的镜像 ${image} `
      shell.echo(pre+'中')
      shell.exec(`docker pull ${image}`)
      shell.echo(pre+'完成')      
    }
  },
}