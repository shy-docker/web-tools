const yargs = require('yargs')
const inquirer = require('inquirer')
const services = ['mysql','visualizer']
/**
 * @typedef {any} argv
 */
module.exports = {
  // command: 'add [server]',
  desc:'添加服务',
  /**@param {yargs} yargs */
  builder(yargs){
    const require = !!yargs.argv.input
    return yargs
    .options('server',{ require, })
  },
  /**@param {argv} argv */  
  async corce(argv){ return argv },
  /**@param {argv} argv */  
  async handler(argv){
    argv = await require(__filename).corce(argv)
    let config = require('./').readDockerConfig()
    let exist_services = Object.keys(config.services)
    let _config = await inquirer.prompt({
      type:'choices',
      choices:services.filter(y=>!exist_services.includes(y)).concat(''),
    })
  },
}