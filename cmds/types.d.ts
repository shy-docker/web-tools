export as namespace Types

export interface DockerServer {
  image: string
}

export interface DockerConfig {
  version: '3' | '3.1'
  services: {
    [key:string]: DockerServer
  }
  volumes: any
}