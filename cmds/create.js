const yargs = require('yargs')
const path = require('path')
const shell = require('shelljs')
const fs = require('fs')
const ssl_info = '/C=CN/ST=Shanghai/L=Shanghai/O=Example Inc./OU=IT Dept.'
const { ssl_dir, vhost_dir } = require('./')
/**
 * @typedef {Object} argv
 * @prop {string} host
 * @prop {boolean} ssl
 */
module.exports = {
  command: 'create [host]',
  desc:'设置新域名',
  /**@param {yargs} yargs */
  builder(yargs){
    return yargs
    .option('host',{
      require: false,
    })
    .option('ssl',{
      type: 'boolean',
      default: true,
    })
  },
  /**@param {argv} argv */  
  async corce(argv){
    const q = require('inquirer')
    let _argv = await q.prompt(/**@type {*}*/([
      {
        name:'host',
        message({ host = argv.host }={}){
          return host ? '请确认域名' : '请输入域名'
        },
        type: 'input',
        default: argv.host,
        /**@param {string} dir */
        validate(dir){
          dir = path.join(vhost_dir,dir)
          if( !fs.existsSync(dir) || !fs.readdirSync(dir).length ){
            return true
          }
          shell.echo(`文件夹 ${dir} 已被使用 \r\n`)
          return false
        },
      },
      {
        name: 'ssl',
        type: 'confirm',
        message: '是否开启 ssl ',
        default: argv.ssl,
      },
    ]))
    return {
      ...argv,
      ..._argv,
    }
  },
  /**@param {argv} argv */
  async handler(argv){
    argv = await require(__filename).corce(argv)
    let dir = path.join(vhost_dir,argv.host)
    let index_file = path.join(dir,'index.html')
    shell.exec(`echo 'hello ${argv.host}' >> ${index_file}`)
    if(argv.ssl){
      shell.exec(`openssl req -new -newkey rsa:2048 -sha256 -nodes -out ${ssl_dir}/${argv.host}.csr -keyout ${ssl_dir}/${argv.host}.key -subj "${ssl_info}/CN=${argv.host}" > /dev/null 2>&1`)
      let filename_prefix = `${ssl_dir}/${argv.host}`
      shell.exec(`openssl x509 -req -days 36500 -sha256 -in ${filename_prefix}.csr -signkey ${filename_prefix}.key -out ${filename_prefix}.crt > /dev/null 2>&1`)
    }

  },
}