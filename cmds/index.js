/// <reference path="./types.d.ts" />

exports.dotenvfile = require('path').join(__dirname,'../.env')
require('dotenv').config({ path: exports.dotenvfile })

exports.dev = /production/.test(process.env.NODE_ENV)

exports.scripts_url = 'https://gitlab.com/shy-docker/web-tools/raw/master'

exports.web_dir = process.env.web_dir || '/web'
exports.config_filepath = `${exports.web_dir}/docker-compose.yml`

exports.nginx_dir = `${exports.web_dir}/nginx/conf.d`
exports.ssl_dir = `${exports.nginx_dir}/ssl`
exports.vhost_dir = `${exports.nginx_dir}/vhost`
exports.rewrite_dir = `${exports.nginx_dir}/rewrite`

const fs = require('fs')
const yaml = require('js-yaml')
/**
 * @returns {Types.DockerConfig}
 */
exports.readDockerConfig = ()=>yaml.safeLoad(fs.readFileSync(exports.config_filepath,'utf8'))
/**@param {JSON} config */
exports.storeDockerConfig = (config)=>fs.writeFileSync(exports.config_filepath,yaml.safeDump(config))
