const yargs = require('yargs')
const shell = require('shelljs')
/**
 * @typedef {any} argv
 */
module.exports = {
  command: 'update',
  desc:'部署更新后的 /web/docker-compose.yml 文件',
  /**@param {yargs} yargs */
  builder(yargs){
    return yargs
  },
  /**@param {argv} argv */  
  async corce(argv){ return argv },
  /**@param {argv} argv */
  async handler(argv){
    argv = await require(__filename).corce(argv)
    shell.exec(`docker stack deploy -c /web/docker-compose.yml web`)
  },
}