const yargs = require('yargs')
const shell = require('shelljs')
/**
 * @typedef {Object} argv
 * @prop {boolean} cn
 */

module.exports = {
  command: 'init',
  desc:'初始化 web 服务',
  /**@param {yargs} yargs */
  builder(yargs){
    return yargs
    .option('cn',{
      desc:'使用中国的软件源',
      type: 'boolean',
      default: false,
    })
  },
  /**@param {argv} argv */  
  async corce(argv){ return argv },
  /**@param {argv} argv */  
  async handler(argv){
    argv = await require(__filename).corce(argv)
    shell.exec('web install docker')
  },
}