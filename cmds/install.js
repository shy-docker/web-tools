const yargs = require('yargs')
module.exports = {
  command:'install',
  desc:'软件多平台统一安装脚本',
  /**@param {yargs} yargs */
  builder(yargs){
    return yargs.commandDir('install')
  }
}