const yargs = require('yargs')
const shell = require('shelljs')
const delay = 2000
/**
 * @typedef {any} argv
 */
module.exports = {
  command: 'restart',
  desc:'重启 docker web 服务',
  /**@param {yargs} yargs */
  builder(yargs){
    return yargs
  },
  /**@param {argv} argv */  
  async corce(argv){ return argv },
  /**@param {argv} argv */
  async handler(argv){
    argv = await require(__filename).corce(argv)
    shell.exec(`docker stack down web`)
    shell.echo(`延迟 ${delay/1e3}s 等待 docker 清理`)
    setTimeout(()=>shell.exec(`web update`),delay)
  },
}