const yargs = require('yargs')
/**
 * @typedef {Object} argv
 */
module.exports = {
  command: 'rm',
  desc:'',
  /**@param {yargs} yargs */
  builder(yargs){},
/**@param {argv} argv */  
  async corce(argv){ return argv },
  /**@param {argv} argv */  
  async handler(argv){
    argv = await require(__filename).corce(argv)
  },
}