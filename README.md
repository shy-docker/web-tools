## 安装

```sh

curl https://gitlab.com/shy-docker/web-tools/raw/master/scripts/web/install.sh | bash

# 国内安装
curl https://gitlab.com/shy-docker/web-tools/raw/master/scripts/web/install-cn.sh | bash

```

## docker 安装

```sh
curl https://gitlab.com/shy-docker/web-tools/raw/master/scripts/docker/install.sh | bash
```

----

## 方法

### web init

初始化 web 环境

- 创建 `/web` 目录存储数据
- 设置为 `docker` 管理集群
- 启动 `nginx` 服务

### web update

应用更新后 `web` 的 `docker-compose.yml` 文件

### web create [vhost] [git-url]

添加新域名

### web add/rm [addons]

- `web add mysql` 添加数据库
- `web add visualizer` 添加监控工具
