# 下载node
NODE_VERSION="v8.10.0"
TAR_NAME="node-${NODE_VERSION}-linux-x64"
curl https://nodejs.org/dist/${NODE_VERSION}/$TAR_NAME.tar.xz | tar xvJf -

# 把node加入环境变量
NODE_BIN="$(pwd)/$TAR_NAME/bin"
echo "export PATH=\"\$PATH:${NODE_BIN}\"" >> ~/.bashrc
# 有些情况下不在这里创建 node 的话, 无法使用 npm
sudo ln -s ${NODE_BIN}/node /usr/bin/node
source ~/.bashrc
