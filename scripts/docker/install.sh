http_url='https://gitlab.com/shy-docker/web-tools/raw/master/'

unspport_linux_issue_echo="只支持 centos7 和 ubuntu . 当前 linux 版本不支持"

# 检查 docker 是否已安装
if (hash docker 2>/dev/null); then
  echo 'docker 已安装. '
  exit 0
fi

if [ -f /etc/redhat-release ]; then
  if [ -z "`grep "/6." /etc/redhat-release`" ]; then
    echo $unspport_linux_issue_echo
    exit 1
  else 
    curl ${http_url}scripts/docker/install-docker-centos7.sh
  fi 
elif [ -f /etc/issue ]; then
  curl ${http_url}scripts/docker/install-docker-ubuntu.sh
else
  echo $unspport_linux_issue_echo
  exit 1
fi
