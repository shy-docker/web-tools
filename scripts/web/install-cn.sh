http_url='https://gitlab.com/shy-docker/web-tools/raw/master/'

# 安装必需的运行环境
curl ${http_url}scripts/web/install-preset.sh | bash

# 添加 cnpm
curl ${http_url}scripts/add-cnpm.sh | bash

cnpm i -g docker-web-tools

curl ${http_url}scripts/web/install-post.sh | bash
