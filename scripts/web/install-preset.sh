http_url='https://gitlab.com/shy-docker/web-tools/raw/master/'

# 检查web是否已安装
if (hash web 2>/dev/null); then
  echo "web 已被安装. 安装路径是: $(type web)"
  exit
fi

# 安装 node
if !(hash node 2>/dev/null); then
  curl ${http_url}scripts/install-node.sh
fi
